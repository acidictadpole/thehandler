use std::{collections::HashSet, env};
use serenity::{
    framework::standard::{
        Args, CommandResult,
        DispatchError, HelpOptions, help_commands, StandardFramework,
        macros::{command, group, help, check},
    },
    model::{channel::{Channel, Message}, gateway::Ready, id::UserId},
    utils::{content_safe, ContentSafeOptions},
};
use serenity::prelude::*;

mod mhwdb;

struct Handler;

impl EventHandler for Handler {
    fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
}

#[group]
#[commands(ping, monster)]
struct Ping;

#[command]
fn ping(ctx: &mut Context, msg: &Message, _args: Args) -> CommandResult {
    if let Err(why) = msg.channel_id.say(&ctx.http, "pong!") {
        println!("Error responding to ping: {:?}", why);
    }
    if let Err(why) = msg.react(&ctx, '👌') {
        println!("Error reacting to ping message: '{}'", why);
    }
    Ok(())
}

#[command]
fn monster(ctx: &mut Context, msg: &Message, mut args: Args) -> CommandResult {
    let subcommand = if args.len() > 0 {args.single::<String>()?} else {"".to_string()};
    let details = args.rest();
    println!("monster args: {:?}, {:?}", subcommand, details);
    let response = match subcommand.to_ascii_lowercase().as_ref() {
        "type" => monsters_of_type(details),
        _ => "I don't know what you're talking about! Try `monster type`.".to_string(),
    };
    if let Err(why) = msg.channel_id.say(&ctx.http, response) {
        println!("Error responding to monster types: {:?}", why);
    }
    Ok(())
}

fn monsters_of_type(monster_type: &str) -> String {
    let unknown = "I only know of bird, brute, fanged, flying, piscine, and elder kinds of monster.";
    if monster_type.len() == 0 {
        return unknown.to_string();
    }
    let response = match monster_type
            .to_ascii_lowercase()
            .replace(" wyvern", "")
            .replace(" dragon", "")
            .as_ref() {
        "bird" => "Kulu-Ya-Ku, Pukei-Pukei, Tzitzi-Ya-Ku, Yian Garuga",
        "brute" => "Anjanath, Banbaro, Barroth, Brachydios, Deviljho, Glavenus, Radobaan, Uragaan",
        "fanged" => "Dodogama, Great Girros, Great Jagras, Odogaron, Rajang, Tobi-Kadachi, Zinogre",
        "flying" => "Barioth, Bazelgeuse, Diablos, Legiana, Nargacuga, Paolumu, Rathalos, Rathian, Tigrex",
        "piscine" => "Beotodus, Jyuratodus, Lavasioth",
        "elder" => "Behemoth, Kirin, Kulve Taroth, Kushala Daora, Lunastra, Namielle, Nergigante, Shara Ishvalda, Teostra, Vaal Hazak, Velkhana, Xeno'jiva, Zorah Magdaros",
        _ => ""
    };
    if response == "" {
        return "Sounds exotic!".to_string() + unknown;
    }
    return "```".to_string() + response + "```";
}

fn main() {
    let token = env::var("DISCORD_TOKEN").expect(
        "Expected a token in the environment",
    );
    let mut client = Client::new(&token, Handler).expect("Err creating client");
    // We will fetch your bot's owners and id
    let (owners, bot_id) = match client.cache_and_http.http.get_current_application_info() {
        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);

            (owners, info.id)
        },
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    client.with_framework(
        StandardFramework::new()
        .configure(|c| c
            .with_whitespace(true)
            .on_mention(Some(bot_id))
            .prefix("~")
            .delimiters(vec![", ", ",", " "])
            .owners(owners))
        .before(|_ctx, msg, command_name| {
            println!("Got command '{}' by user '{}'",
                    command_name,
                    msg.author.name);
            true
        })
        .after(|_, _, command_name, error| {
            match error {
                Ok(()) => println!("Processed command '{}'", command_name),
                Err(why) => println!("Command '{}' returned error {:?}", command_name, why),
            }
        })
        .unrecognised_command(|_, _, unknown_command| {
            println!("Could not find command name '{}'", unknown_command);
        })
        .on_dispatch_error(|ctx, msg, error| {
            if let DispatchError::Ratelimited(seconds) = error {
                let _ = msg.channel_id.say(&ctx.http, &format!("Try this again in {} seconds.", seconds));
            }
        })
        .group(&PING_GROUP)
    );
    if let Err(why) = client.start(){
        println!("Client error: {:?}", why);
    }
}
