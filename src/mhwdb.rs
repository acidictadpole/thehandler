pub mod mhwdb {
  use chrono::{DateTime, Utc};
  use serde::{Deserialize, Serialize};
  use serde_json::Result;

  trait DataFetcher {
    fn get_events() -> Vec<Event>;
  }

  #[derive(Serialize, Deserialize, PartialEq, Debug)]
  pub struct Camp {
    id: u32,
    name: String,
    zone: u8,
  }

  impl Camp {
    pub fn new(id: u32, name: String, zone: u8) -> Self {
      Camp { id, name, zone }
    }
  }

  //https://docs.mhw-db.com/#location-fields
  #[derive(Serialize, Deserialize, PartialEq, Debug)]
  pub struct Location {
    id: u32,
    name: String,
    zone_count: u8,
    camps: Vec<Camp>,
  }

  impl Location {
    pub fn new(id: u32, name: String, zone_count: u8, camps: Vec<Camp>) -> Self {
      Location {
        id,
        name,
        zone_count,
        camps,
      }
    }
  }

  //https://docs.mhw-db.com/#event-fields
  #[derive(Serialize, Deserialize, PartialEq, Debug)]
  pub struct Event {
    id: u32,
    name: String,
    platform: String,  //Technically its own type but it appears as a string.
    exclusive: String, // Same
    r#type: String,    // Same
    expansion: String, // Same
    description: String,
    requires: String,
    quest_rank: u8,
    success_conditions: String,
    start_timestamp: DateTime<Utc>,
    end_timestamp: DateTime<Utc>,
    location: Location,
  }

  impl Event {
    pub fn new(
      id: u32,
      name: String,
      platform: String,
      exclusive: String,
      r#type: String,
      expansion: String,
      description: String,
      requires: String,
      quest_rank: u8,
      success_conditions: String,
      start_timestamp: DateTime<Utc>,
      end_timestamp: DateTime<Utc>,
      location: Location,
    ) -> Self {
      Event {
        id,
        name,
        platform,
        exclusive,
        r#type,
        expansion,
        description,
        requires,
        quest_rank,
        success_conditions,
        start_timestamp,
        end_timestamp,
        location,
      }
    }
  }
}


#[cfg(test)]
mod tests {
  use super::mhwdb::*;
  #[test]
  fn camp_to_json() {
    let camp = Camp::new(2, String::from("MyCamp"), 2);
    let js = serde_json::to_string(&camp).expect("Couldn't parse camp to json.");
    let expected = r#"{"id":2,"name":"MyCamp","zone":2}"#;
    assert_eq!(js, expected);
  }

  #[test]
  fn camp_from_json(){
    let js = r#"{"id":5,"name":"FooCamp","zone":4}"#;
    let camp: Camp = serde_json::from_str(js).expect("Couldn't parse camp from json");
    let expected = Camp::new(5, String::from("FooCamp"), 4);
    assert_eq!(camp, expected);
  }
}